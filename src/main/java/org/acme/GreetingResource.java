package org.acme;

import lombok.extern.slf4j.Slf4j;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Slf4j
@Path("/hello-resteasy")
public class GreetingResource {

    @Inject
    Work work;

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String hello() {
        log.info("Processing request.");

        for(int i = 0; i < 10; i++) {
            work.doWork();
        }
        return "Hello RESTEasy";
    }
}